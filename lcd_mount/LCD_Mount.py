# -------------------------------------------------------
# Created by Troy Franks
# 2022-12-21 Rev 1
# LCD Mount
# -------------------------------------------------------
import cadquery as cq

id_height = 7.62  # .300"

id_width = 78.74  # 3.1"

id_thickness = 13.208  # .520"

wall_thickness = 5.16  # .203"

mounting_tab_width = 12.7  # .50"
# mount_hole_pattern = 12.0  # .472"
id_padding = 0.5  # .020"

# Hole properties
mount_hole_dia = 3
csink_angle = 45
csink_dia = mount_hole_dia + 1
csink_depth = None


def mount_hole():
    cq.Workplane("XY").cskHole(2, 5, 45, 5)


# make inside
bracket_id = (
    cq.Workplane("XY")
    .box(
        id_width + id_padding,
        id_height,
        id_thickness,
    )
    .translate(
        (
            0,
            0,
            -(wall_thickness / 2) / 2,
        )
    )
)

# make tabs
mount_tab = cq.Workplane("XY").box(
    mounting_tab_width,
    id_height + 25.4,
    id_thickness,
)

# mount_hole = cq.Workplane("XY").cskHole(4, 5, 45, 10)

mount_tab_right = mount_tab.translate(
    (
        ((id_width / 2) + mounting_tab_width)
        + (wall_thickness / 2)
        - (mounting_tab_width / 2),
        0,
        (wall_thickness / 2) / 2,
    ),
)

mount_tab_left = mount_tab.translate(
    (
        -(
            (id_width / 2)
            + mounting_tab_width
            + (wall_thickness / 2)
            - (mounting_tab_width / 2)
        ),
        0,
        (wall_thickness / 2) / 2,
    ),
)


# make the base

result = (
    cq.Workplane("XY")
    .box(
        id_width + wall_thickness + (mounting_tab_width * 2),
        id_height,
        id_thickness + (wall_thickness / 2),
    )
    .faces(">Z")
    .workplane()
    .cut(bracket_id)
    .faces(">Z")
    .workplane()
    .cut(mount_tab_right)
    .faces(">Z")
    .workplane()
    .cut(mount_tab_left)
    .faces(">Z")
    .workplane(offset=-id_thickness)
    .moveTo(
        x=(id_width / 2) + (wall_thickness / 2) + (mounting_tab_width / 2),
        y=0,  # (id_height / 2) / 2,
    )
    .cskHole(
        diameter=mount_hole_dia,
        cskDiameter=csink_dia,
        cskAngle=csink_angle,
        depth=None,
    )
    # .moveTo(
    #     x=(id_width / 2) + (wall_thickness / 2) + (mounting_tab_width / 2),
    #     y=-((id_height / 2) / 2),
    # )
    # .cskHole(
    #     diameter=mount_hole_dia,
    #     cskDiameter=csink_dia,
    #     cskAngle=csink_angle,
    #     depth=csink_depth,
    # )
    .moveTo(
        x=-((id_width / 2) + (wall_thickness / 2) + (mounting_tab_width / 2)),
        y=0,  # (id_height / 2) / 2,
    )
    .cskHole(
        diameter=mount_hole_dia,
        cskDiameter=csink_dia,
        cskAngle=csink_angle,
        depth=None,
    )
    # .moveTo(
    #     x=-((id_width / 2) + (wall_thickness / 2) + (mounting_tab_width / 2)),
    #     y=-((id_height / 2) / 2),
    # )
    # .cskHole(
    #     diameter=mount_hole_dia,
    #     cskDiameter=csink_dia,
    #     cskAngle=csink_angle,
    #     depth=csink_depth,
    # )
)

# Render the solid

show_object(result)

# Export

cq.exporters.export(result, "export_mount/LCD_Mount.stl")

# cq.exporters.export(result.section(), "export_bottom_bracket/bottom_bracket.dxf")

# cq.exporters.export(result, "export_bottom_bracket/bottom_bracket.step")
