#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Add project comments here
"""

# Imports
import cadquery as cq

# Personal Imports

__author__ = "Troy Franks"
__version__ = "2023-09-01"


# Cover Properties
cover_oa_x: float = 78.74  # 3.1"
cover_cord_length: float = 44.45  # 1.750
cover_oa_y: float = 76.073  # 2.995"
cover_oa_z: float = 19.685  # 1.0"
slot_location: float = 21.59  # .850"
slot_width: float = 3.175  # .125"
wall_thickness = 5.16  # .203"

cord_clearance_notch_width: float = 3.5  # .138"
cord_clearance_notch_y: float = 21.59  # .850"

# Mounting Hole Properties
mount_holes_distance: float = 58.166  # 2.290"
mount_hole_dia: float = 2.0  # .078"
csink_angle = 45
csink_dia = mount_hole_dia + 1
csink_depth = None
mount_hole_loc_y: float = 62.23  # 2.450"
mount_hole_loc_x: float = (
    (cover_oa_x + wall_thickness) - mount_holes_distance
) / 2

# Catch Properties
bottom_catch_dist_z: float = 1.651  # .065"
catch_dist_y: float = 1.651  # .065"
bottom_catch_thickness: float = 1.651  # .065
bottom_catch_slot_width: float = 1.575  # .062"
bottom_catch_slot_length: float = 12.7  # .50"
bottom_catch_slot_in_sides: float = 12.7

# Padding & Adjustments
undercut_thickness_padding = 1.27  # .05"
oay_padding = 0.254  # .010"

# Overall height. Allow for  catches distances to be translated
oah: float = cover_oa_z + bottom_catch_thickness + bottom_catch_dist_z


# The ID logic
cover_id = (
    cq.Workplane("XY")
    .box(
        cover_oa_x,
        cover_cord_length,
        cover_oa_z,
        centered=False,
    )
    .translate(
        (
            wall_thickness / 2,
            (wall_thickness / 2),
            bottom_catch_thickness + bottom_catch_dist_z,
        )
    )
)

# Cord clearance notch
cord_clearance_notch = (
    cq.Workplane("XY")
    .box(
        wall_thickness,
        cord_clearance_notch_width,
        cord_clearance_notch_width,
        centered=False,
    )
    .translate(
        (
            0,
            cord_clearance_notch_y - (cord_clearance_notch_width / 2),
            bottom_catch_thickness + bottom_catch_dist_z,
        )
    )
)

# Undercut logic on the top tha creates the window
cover_window = (
    cq.Workplane("XY")
    .box(
        cover_oa_x + wall_thickness,
        (cover_oa_y - cover_cord_length) + 5,
        cover_oa_z + wall_thickness,
        centered=False,
    )
    .translate(
        (
            0,
            cover_cord_length + (wall_thickness / 2),
            (wall_thickness + undercut_thickness_padding),
        ),
    )
)

# Catch tab logic
catch_profile = (
    cq.Workplane("XY")
    .box(
        cover_oa_x + wall_thickness,
        cover_oa_y,
        bottom_catch_dist_z,
        centered=False,
    )
    .translate(
        (
            0,
            wall_thickness / 2,
            bottom_catch_thickness,  # -(wall_thickness / 2) / 2,
        )
    )
)

catch_clear_big = (
    cq.Workplane("XY")
    .box(
        cover_oa_x + wall_thickness,
        cover_oa_y - (catch_dist_y * 2),
        bottom_catch_dist_z,
        centered=False,
    )
    .translate(
        (
            0,
            (wall_thickness / 2) + catch_dist_y,
            0,  # -(wall_thickness / 2) / 2,
        )
    )
)

# Remvoe catch on the sides bottom logic
catch_clear_bottom = cq.Workplane("XY").box(
    bottom_catch_slot_in_sides + bottom_catch_slot_width,
    cover_oa_y - catch_dist_y,
    bottom_catch_dist_z,
    centered=False,
)

catch_clear_bottom_left = catch_clear_bottom.translate(
    (
        0,
        (wall_thickness / 2),
        0,
    )
)

catch_clear_bottom_right = catch_clear_bottom.translate(
    (
        (cover_oa_x + (wall_thickness)) - (bottom_catch_slot_in_sides),
        (wall_thickness / 2),
        0,
    )
)

# Slot bottom logic
slots_bottom = cq.Workplane("XY").box(
    bottom_catch_slot_width,
    wall_thickness,
    bottom_catch_slot_length,
    centered=False,
)

slot_bottom_left = slots_bottom.translate(
    (
        bottom_catch_slot_in_sides,
        0,
        0,  # -(wall_thickness / 2) / 2,
    )
)

slot_bottom_right = slots_bottom.translate(
    (
        (cover_oa_x + (wall_thickness / 2) + bottom_catch_slot_width)
        - bottom_catch_slot_in_sides,
        0,
        0,  # -(wall_thickness / 2) / 2,
    )
)


# make the base

# Main Box
result = cq.Workplane("XY").box(
    cover_oa_x + wall_thickness,
    cover_oa_y + oay_padding + wall_thickness,
    oah + wall_thickness / 2,
    centered=False,
)

# Mounting Hole Left
result = (
    result.faces(">Z")
    .workplane(offset=-(cover_oa_z - (wall_thickness) / 2))
    .moveTo(
        x=mount_hole_loc_x,
        y=mount_hole_loc_y,
    )
    .cskHole(
        mount_hole_dia,
        csink_dia,
        csink_angle,
        csink_depth,
    )
)

# Mounting Hole Right
result = result.moveTo(
    x=mount_hole_loc_x + mount_holes_distance,
    y=mount_hole_loc_y,
).cskHole(
    mount_hole_dia,
    csink_dia,
    csink_angle,
    csink_depth,
)


# Case ID
result = result.faces(">Z").workplane().cut(cover_id)

# Case Window
result = result.faces(">Z").workplane().cut(cover_window)

#  Catch Feature Bottom
result = result.faces(">Y").workplane().cut(catch_profile)

#  Catch Clear 1st
result = result.faces(">Y").workplane().cut(catch_clear_big)

#  Catch Clear Bottom Left
result = result.faces(">Y").workplane().cut(catch_clear_bottom_left)

#  Catch Clear Bottom Right
result = result.faces(">Y").workplane().cut(catch_clear_bottom_right)

# Cord Clearance Notch
result = result.faces(">Y").workplane().cut(cord_clearance_notch)

#  Slot Left
result = result.faces("top").workplane().cut(slot_bottom_left)

#  Slot Right
result = result.faces("top").workplane().cut(slot_bottom_right)


# Render the solid

show_object(result)

# Export

cq.exporters.export(result, "export_cover/rp_clock_cover.stl")

cq.exporters.export(result.section(), "export_cover/rp_clock_cover.dxf")

cq.exporters.export(result, "export_cover/rp_clock_cover.step")


if __name__ == "__main__":
    pass
