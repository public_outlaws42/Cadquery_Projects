# -------------------------------------------------------
# Created by Troy Franks
# 2022-12-21 Rev 1
# CNC Router Power Supply Bottom Bracket
# -------------------------------------------------------

import cadquery as cq

id_height = 19.05

id_width = 127

id_thickness = 31.75

wall_thickness = 10.16

mounting_tab_width = 25.4
mount_hole_pattern = 12.0
id_padding = 0.5


def mount_hole():
    cq.Workplane("XY").cskHole(2, 5, 45, 5)


# make inside
bracket_id = (
    cq.Workplane("XY")
    .box(
        id_width + id_padding,
        id_height + wall_thickness / 2,
        id_thickness,
    )
    .translate(
        (
            0,
            wall_thickness / 2,
            -(wall_thickness / 2) / 2,
        )
    )
)

# make tabs
mount_tab = cq.Workplane("XY").box(
    mounting_tab_width,
    id_height + 25.4,
    id_thickness,
)

# mount_hole = cq.Workplane("XY").cskHole(4, 5, 45, 10)

mount_tab_right = mount_tab.translate(
    (
        ((id_width / 2) + mounting_tab_width)
        + (wall_thickness / 2)
        - (mounting_tab_width / 2),
        0,
        (wall_thickness / 2) / 2,
    ),
)

mount_tab_left = mount_tab.translate(
    (
        -(
            (id_width / 2)
            + mounting_tab_width
            + (wall_thickness / 2)
            - (mounting_tab_width / 2)
        ),
        0,
        (wall_thickness / 2) / 2,
    ),
)


# make the base

result = (
    cq.Workplane("XY")
    .box(
        id_width + wall_thickness + (mounting_tab_width * 2),
        id_height + wall_thickness / 2,
        id_thickness + (wall_thickness / 2),
    )
    .faces(">Z")
    .workplane()
    .cut(bracket_id)
    .faces(">Z")
    .workplane()
    .cut(mount_tab_right)
    .faces(">Z")
    .workplane()
    .cut(mount_tab_left)
    .faces(">Z")
    .workplane(offset=-id_thickness)
    .moveTo(
        (id_width / 2) + (wall_thickness / 2) + (mounting_tab_width / 2),
        (id_height / 2) / 2,
    )
    .cskHole(4, 6, 45, 50)
    .moveTo(
        (id_width / 2) + (wall_thickness / 2) + (mounting_tab_width / 2),
        -((id_height / 2) / 2),
    )
    .cskHole(4, 6, 45, 50)
    .moveTo(
        -((id_width / 2) + (wall_thickness / 2) + (mounting_tab_width / 2)),
        (id_height / 2) / 2,
    )
    .cskHole(4, 6, 45, 50)
    .moveTo(
        -((id_width / 2) + (wall_thickness / 2) + (mounting_tab_width / 2)),
        -((id_height / 2) / 2),
    )
    .cskHole(4, 6, 45, 50)
)

# Render the solid

show_object(result)

# Export

cq.exporters.export(result, "export_bracket/bottom_bracket.stl")

# cq.exporters.export(result.section(), "export_bottom_bracket/bottom_bracket.dxf")

# cq.exporters.export(result, "export_bottom_bracket/bottom_bracket.step")
