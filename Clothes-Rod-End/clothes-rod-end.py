#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Clothes rod end to keep the hangers from coming off the end.
"""

# Imports
import cadquery as cq

# Personal Imports

__author__ = "Troy Franks"
__version__ = "2024-02-12"

# Head Properties
Head_Diameter: float = 67.691  # 2.665"
Head_Length: float = 6.35  # .250"

# Shank Properties
Shank_Diameter: float = 30.226  # 1.190"
Shank_Length: float = 25.4  # 1.0"
Taper_Length_Z: float = 12.7  # 1.0"
Taper_Length_X: float = 0.3  # .012"

# Fit Properties
Fit_Adjust: float = 0.254  # .010"

Shank_Diameter_Fit = Shank_Diameter + Fit_Adjust


# make the base

# Head
result = (
    cq.Workplane("XY")
    .moveTo(
        x=-(Head_Diameter / 2),
        y=-(Head_Diameter / 2),
    )
    .cylinder(
        Head_Length,
        Head_Diameter / 2,
        centered=False,
    )
)

# Shank
result = (
    result.faces("+Z")
    .workplane(offset=0)
    .moveTo(
        x=-(Shank_Diameter_Fit / 2),
        y=-(Shank_Diameter_Fit / 2),
    )
    .cylinder(
        Shank_Length,
        Shank_Diameter_Fit / 2,
        centered=False,
    )
)

# Chamfer 30 Deg
result = result.faces(">Z").chamfer(Taper_Length_Z, Taper_Length_X)

# Render the solid
show_object(result)

# Export

cq.exporters.export(result, "export/Clothes-Rod-End.stl")
cq.exporters.export(result, "export/Clothes-Rod-End.step")
